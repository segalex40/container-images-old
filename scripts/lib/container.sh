# container::login "quay.io" "user" "pass"
container::login() {
    local URL=$1
    local USERNAME=$2
    local PASSWORD=$3

    buildah login --username "${USERNAME}" --password "${PASSWORD}" "${URL}"
}

# container::logout "quay.io"
container::logout() {
    local URL=$1

    buildah logout ${URL}
}

# container::build "images/autosd"
container::build() {
    # local var init
    local DIR=$1
    local IMG_NAME=$(basename $DIR)

    buildah bud \
        --file ${DIR}/Containerfile \
        --tag ${IMG_REG}/${IMG_ORG}/${IMG_NAME}:${IMG_TAG} \
        ${BUILD_OPTS} \
        ${DIR}
}

# container::push "images/autosd"
container::push() {
    # local var init
    local DIR=$1
    local IMG_NAME=$(basename $DIR)
    
    buildah push ${IMG_REG}/${IMG_ORG}/${IMG_NAME}:${IMG_TAG}
}

# container::remove "images/autosd"
container::remove() {
    local DIR=$1
    local IMG_NAME=$(basename $DIR)

    buildah rmi -f ${IMG_REG}/${IMG_ORG}/${IMG_NAME}:${IMG_TAG}
}

# container::remove_remote "images/autosd"
container::remove_remote() {
    local IMG_NAME=$(basename $DIR)

    skopeo --debug delete \
        --creds "$REG_USERNAME:$REG_PASSWORD" \
        docker://${IMG_REG}/${IMG_ORG}/${IMG_NAME}:${TMP_TAG}
}
